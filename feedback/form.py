from django.forms import ModelForm, TextInput, EmailInput, Textarea
from .models import Feedback as Feedback_model

class Feedback(ModelForm):
    class Meta:
        model = Feedback_model
        fields = ['name', 'email', 'text']
        widgets = {
            'name': TextInput(attrs={'placeholder': 'Ваше имя'}),
            'email': EmailInput(attrs={'placeholder': 'e-mail для обратной связи'}),
            'text': Textarea(attrs={'placeholder': 'Введите сообщение'})
        }