from django.shortcuts import render
from .form import Feedback as Feedback_form

# Create your views here.


def about(request):
    """
    simple render page
    :param request:
    :return: html with form
    """
    feedback = Feedback_form
    return render(request, 'feedback/about.html', {'feedback_form': feedback})


def feedback(request):

    """
    Get POST request, valid form, and create a model object with a feedback =)
    :param request: get POST
    :return: html with form, or error
    """
    feedback = Feedback_form
    if request.method == 'POST':
        form = Feedback_form(request.POST)
        if form.is_valid():
            form.save()
            return render(request, 'feedback/about.html',
                          {'sended': 'Спасибо за уделённое время! Ваш отзыв успешно отправлен.'})
        else:
            return render(request, 'feedback/about.html',
                          {'error': 'Произошла ошибка, обновите страницу и попробуйте снова.'})
    return render(request, 'feedback/about.html', {'feedback_form': feedback})
