# ANKI deck maker

ANKI deck maker is an application which make life easier =) Create a deck fast and efficiently

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install requirements.

```bash
pip install -r requirements.txt
```

## Usage

Just create a txt file with list of english words and upload it

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://choosealicense.com/licenses/mit/)