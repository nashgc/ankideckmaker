$(document).ready(function () {

    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }


    $("#send").on("click", function () {
        $('#deck_link, #error_msg').hide();
        var formData = new FormData($('form').get(0));
        $('#loading-indicator').show();
        $.ajaxSetup({
            headers: {"X-CSRFToken": getCookie("csrftoken")}
        });
        $.ajax({
            url: upload_url,
            type: 'POST',
            data: formData,
            async: false,
            success: function (data) {
                $('#loading-indicator').hide();
                $('body').html(data);
            },
            cache: false,
            contentType: false,
            processData: false,
        });
        return false;
    });
});

