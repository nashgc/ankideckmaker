from django.shortcuts import render, HttpResponse

from .form import UploadForm
from django.http import HttpResponse, Http404
from .deck_maker.parser import parse_file
from django.template.loader import render_to_string
# Create your views here.


def home(request):
    """
    Just render home page with form
    :param request:
    :return: render with form
    """
    form = UploadForm
    return render(request, 'home/home.html', {'form': form})


def upload(request):
    """
    Simple check upload form, call parse_file func and render result
    :param request:
    :return: render with form and dict
    """
    if request.is_ajax():
        form = UploadForm(request.POST, request.FILES)
        if form.is_valid():
            if request.FILES['file'].content_type == 'text/plain':
                # TODO: add a url path to the file in a media dir
                deck_name = parse_file(request.FILES['file'])
                # return render(request, 'home/home.html', {'form': form, 'deck_link': deck_name})
                # file_parser(request, request.FILES['file'])
                html = render_to_string('home/home.html', {'form': form, 'deck_link': deck_name})
                return HttpResponse(html)
            else:
                html = render(request, 'home/home.html', {'form': form, 'error_msg': 'The file is not txt'})
                return HttpResponse(html)
        else:
            html = render(request, 'home/home.html', {'form': form, 'error_msg': 'Please choose a text file'})
            return HttpResponse(html)
    else:
        # return render(request, 'home/home.html')
        form = UploadForm
        return render(request, 'home/home.html', {'form': form})


def download(request, file_name):
    """
    Download a deck zip file.
    :param request:
    :param file_name: get a file name from urls path
    :return: response with nginx Accel-Redirect 1111
    """
    response = HttpResponse()
    response['Content-type'] = 'application/zip; charset=utf-8'
    response['Content-Disposition'] = "attachment; filename={0}".format(file_name)
    response['X-Accel-Redirect'] = "/protected/{0}".format(file_name)
    return response
    raise Http404


def instruction(request):
    return render(request, 'subpages/instruction.html')
