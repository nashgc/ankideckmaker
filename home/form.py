from django import forms

class UploadForm(forms.Form):
    file = forms.FileField(widget=forms.FileInput(attrs={'class': 'custom-file-input', 'id': 'inputGroupFile03',
                                                         'aria-describedby': 'inputGroupFileAddon03'}))
    # TODO: Use it with multispider
    select = forms.ChoiceField(choices = [('RUEN', 'РУС -> АНГЛ')],
                               widget=forms.Select(attrs = {'class': 'custom-select', 'id': 'inputGroupSelect01'}))

    # attrs = {'class': 'custom-select', 'id': 'inputGroupSelect01'}
    # choices = [(1, 'первый'), (2, 'второй'), (3, 'третий')]
