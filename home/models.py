from django.db import models

# Create your models here.

class ProxyPorts(models.Model):
    socks_port = models.IntegerField(blank=True, null=True)
    control_port = models.IntegerField(blank=True, null=True)