import zipfile, random, os, shutil
from time import sleep

import chardet
from stem import Signal
from stem.control import Controller
from stem.process import launch_tor_with_config

from django.conf import settings
from home.models import ProxyPorts

from .genanki import genanki
from .words_class import MyWords


MEDIA_DIR = os.path.join(settings.BASE_DIR, 'media')
DATA_DIR = os.path.join(settings.BASE_DIR, 'data')
mp3_directory = os.path.join(MEDIA_DIR , 'collection_media')
DECKS_DIR = os.path.join(MEDIA_DIR, 'decks')


def get_proxy_port():
    flag = False
    while flag == False:
        port = str(random.randrange(9000, 9999))
        if not ProxyPorts.objects.filter(socks_port=port).exists():
            p = ProxyPorts(socks_port=port)
            p.save()
            return port
            flag = True


def get_control_port():
    flag = False
    while flag == False:
        port = str(random.randrange(9000, 9999))
        if not ProxyPorts.objects.filter(control_port=port).exists():
            p = ProxyPorts(control_port=port)
            p.save()
            return port
            flag = True


def change_identify(control_port):
    with Controller.from_port(port=control_port) as controller:
        controller.authenticate("P2ssw0rd")
        controller.signal(Signal.NEWNYM)
    sleep(3)


def delete_tor_ports(sock_port, control_port):
    s = ProxyPorts.objects.filter(socks_port=sock_port)
    s.delete()
    c = ProxyPorts.objects.filter(control_port=control_port)
    c.delete()


def create_tor_data_dir(port):
    os.mkdir(os.path.join(DATA_DIR, f'data_dir_{port}'))
    return os.path.join(DATA_DIR, f'data_dir_{port}')


def delete_tor_data_dir(port):
    shutil.rmtree(os.path.join(DATA_DIR, f'data_dir_{port}'))


def parse_file(file):
    """
    Simple take a txt file and produce a list of words instances,
    then call make_deck func and return result from it.

    :param file: get a txt with words
    :produce: a list of words
    :return: result form make_deck func
    """
    list = []
    i = 0
    proxy_port = get_proxy_port()
    control_port = get_control_port()
    tor_data_dir = create_tor_data_dir(port=proxy_port)
    tor = launch_tor_with_config(tor_cmd='tor', config={
        'SOCKSPort': proxy_port,
        'ControlPort': control_port,
        'CookieAuthentication': '1',
        'HashedControlPassword': '16:EC3DD433058CFAD560DFA547F6E79D9CAC0EC55D8BA0FCA692651AF25B',
        'DataDirectory': tor_data_dir
    })
    for f in file:
        for word in f.lower().split():
            cod = chardet.detect(word)
            word = word.decode(cod['encoding']).encode('utf-8').decode('utf-8')
            if i == 10:
                change_identify(int(control_port))
                list.append(MyWords(word, proxy_port))
                i += 1
            elif i == 20:
                break
            else:
                list.append(MyWords(word, proxy_port))
                i += 1
                sleep(random.randint(2,5))
    tor.terminate()
    sleep(1)
    delete_tor_ports(sock_port=proxy_port, control_port=control_port)
    delete_tor_data_dir(proxy_port)
    res = make_apkg(list)
    return res



# TODO: APKG is HERE!
def make_apkg(my_list):
    """
    Get a object's list, then use genanki module for apkg generating according to the list.

    :param my_list: list of word's objects
    :return: apkg file name
    """
    qfmt = """<a class='adm' href='http://ankideckmaker.ru'></a>

<br>

{{translation}} <br> {{picture}}
    """
    afmt = """<a class='adm' href='http://ankideckmaker.ru'></a>

<br>
    
{{translation}} <br> {{picture}}

<hr id=answer>

{{infinitive}} <br> {{transcription}} <br> {{sound}}
    """

    def get_hash():
        """
        Simple ID generator
        :return: random int
        """
        return random.randrange(1 << 30, 1 << 31)
    my_model = genanki.Model(
        get_hash(),
        'ankideckmaker.ru',
        fields=[
            {'name': 'translation'},
            {'name': 'transcription'},
            {'name': 'infinitive'},
            {'name': 'picture'},
            {'name': 'sound'},
        ],
        templates=[
            {"name": "Card 1",
             "qfmt": qfmt,
             "afmt": afmt,
             }
        ],
        css=""".card {
 font-family: arial;
 font-size: 20px;
 text-align: center;
 color: black;
 background-color: white;
}
.adm{
 position: absolute;
 top: 7px;
 right: 27px;
 background: url('_adm.png') top right no-repeat;
 display: block;
 width:100px;
 height:50px;
}
""",

    )


    my_deck = genanki.Deck(
        get_hash(),
        'ankideckmaker.ru'
    )

    my_package = genanki.Package(my_deck)

    # Iterate trough a list of words's objects: make note -> add note -> add mp3 if exist -> repeat =)
    for word in my_list:
        my_note = genanki.Note(
            model=my_model,
            fields=[word.translation, word.transcription, word.infinitive, word.picture, word.sound]
        )
        my_deck.add_note(my_note)
        if word.sound != 'mp3_not_found':
            my_package.media_files.append('{}.mp3'.format(word.infinitive))
    my_package.media_files.append('_adm.png')
    file_name = "deck_" + str(random.randint(1, 1000)) + ".apkg"
    my_package.write_to_file(os.path.join(DECKS_DIR, file_name), media_path=mp3_directory)
    return file_name
