import os

import requests
from bs4 import BeautifulSoup

from django.conf import settings
from .user_agent import get_user_agent

class MyWords(object):

    MEDIA_DIR = os.path.join(settings.BASE_DIR, 'media')
    mp3_directory = os.path.join(MEDIA_DIR, 'collection_media')
    mp3_web_path = "https://wooordhunt.ru/data/sound/word/us/mp3/"
    web_word_path = "https://wooordhunt.ru/word/"



    def __init__(self, infinitive, port):
        self.infinitive = infinitive
        self.translation = None
        self.transcription = None
        self.picture = None
        self.sound = None
        self.proxies = {
            'http': f'socks5://127.0.0.1:{port}',
            'https': f'socks5://127.0.0.1:{port}'
        }

        self.scrap_data(self.infinitive)
        self.mp3_download(infinitive)



    def scrap_data(self, infinitive):
        """
        This function get a word and then does a request
        if request_code = 200 it scraps the data from site,
        strip it and assigned it to a properties.
        :param infinitive: get a word
        :return: nothing
        """
        url = self.web_word_path + infinitive
        data_r = requests.get(url, allow_redirects=False,
                              headers={'User-Agent': get_user_agent()}, proxies=self.proxies)
        if data_r.status_code != 200:
            self.translation = 'слово не найдено'
            self.transcription = infinitive
            self.picture = infinitive
        else:
            data_soup = BeautifulSoup(data_r.text, "html.parser")
            transcription = data_soup.find("span", {"title": "американская транскрипция слова {}".format(infinitive)})
            translation = data_soup.find("span", {"class": "t_inline_en"})
            try:
                transcription = transcription.text.strip()
                translation = translation.text.strip()
            except:
                self.translation = 'NONE'
                self.transcription = infinitive
                self.picture = infinitive
            else:
                self.translation = translation
                self.transcription = transcription
                self.picture = '<img src="{}.jpg" />'.format(infinitive)


    def mp3_download(self, infinitive):
        """
        This function gets a word and check file,
        if file doesn't exist, try to do a request,
        if status_code = 200 it downloads a file
        and write it to mp3_directory.
        :param infinitive:  it gets a word
        :return: nothing
        """
        if os.path.isfile(self.mp3_directory + infinitive + ".mp3"):
            self.sound = '[sound:{}.mp3]'.format(infinitive)
        else:
            url = self.mp3_web_path + infinitive + ".mp3"
            data_r = requests.get(url, allow_redirects=False,
                                  headers={'User-Agent': get_user_agent()}, proxies=self.proxies)
            if data_r.status_code != 200:
                self.sound = 'mp3_not_found'
            else:
                local_path = os.path.join(self.mp3_directory, '{}.mp3'.format(infinitive))
                with open(local_path, "wb") as local_file:
                    local_file.write(data_r.content)
                    self.sound = '[sound:{}.mp3]'.format(infinitive)
